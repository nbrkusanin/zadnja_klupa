$(document).ready(function() {
    $(window).scroll(function() {
        let position = $(this).scrollTop();
        if (position > 40) {
            $(".language").slideUp("slow");
            $('.navbar').addClass('navAnimate');
        } else {
            $(".language").slideDown("fast");
            $('.navbar').removeClass('navAnimate');
        }
    });

    $('.sliderNext').on('click', function() {
        let currentImg = $('.active');
        let nextImg = currentImg.next();

        if (nextImg.length) {
            currentImg.removeClass('active').css('z-index', -10);
            nextImg.addClass('active').css('z-index', 10);
        }
    });

    $('.sliderPrev').on('click', function() {
        let currentImg = $('.active');
        let prevImg = currentImg.prev();

        if (prevImg.length) {
            currentImg.removeClass('active').css('z-index', -10);
            prevImg.addClass('active').css('z-index', 10);
        }
    });

    $('.sliderImg1 img').on('click', function() {
        let currentImg = $('.active');
        currentImg.removeClass('active');
        $(this).clone().prependTo('.sliderImg').attr('class', 'active');
        // let currentImg = $('.active');
        // currentImg.removeClass('active');
        //$(this).addClass('active');
    });
});