$(document).ready(function() {
    $(window).scroll(function() {
        let position = $(this).scrollTop();
        if (position > 40) {
            $(".language").slideUp("slow");
            $('.navbar').addClass('navAnimate');
        } else {
            $(".language").slideDown("fast");
            $('.navbar').removeClass('navAnimate');
        }
    });

    $('.btnContact').click(function(event) {


        let email = $('#emailInput').val();
        let subject = $('#subjectInput').val();
        let textArea = $('#textAreafield').val();

        let statusElement = $('.status');
        statusElement.empty();

        if (email.length > 5 && email.includes('@') && email.includes('.')) {

        } else {
            event.preventDefault();
            statusElement.append('<p style="color:red;">Vas email nije validan</p>');
            $('#emailInput').css('border-color', 'red');
        }

        if (subject.length >= 2) {

        } else {
            event.preventDefault();
            statusElement.append('<p p style="color:red;">Vas Subject nije validan</p>');
            $('#subjectInput').css('border-color', 'red');
        }

        if (textArea.length >= 5) {

        } else {
            event.preventDefault();
            statusElement.append('<p p style="color:red;">Vas tekst mejla nije validan</p>');
            $('#textAreafield').css('border-color', 'red');
        }
    });

});

const cats = [
    "../img/studenica3.jpg",
    "../img/slapovi.jpg",
    "../img/RUDNO_21_s.jpg"
]

const node = document.getElementById("frontPage");

const cycleImages = (images, container, step) => {
    images.forEach((image, index) => (
        setTimeout(() => {
            container.style.backgroundImage = `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.7)), url(${image})`
        }, step * (index + 1))
    ))
    setTimeout(() => cycleImages(images, container, step), step * images.length)
}

cycleImages(cats, node, 3000);