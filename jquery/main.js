// navbar on scroll

$(document).ready(function() {
    $(window).scroll(function() {
        let position = $(this).scrollTop();
        if (position > 40) {
            $(".language").slideUp("slow");
            $('.navbar').addClass('navAnimate');
        } else {
            $(".language").slideDown("fast");
            $('.navbar').removeClass('navAnimate');
        }
    });
});

//navbar changing img
const cats = [
    "./img/studenica3.jpg",
    "./img/slapovi.jpg",
    "./img/RUDNO_21_s.jpg"
]

const node = document.getElementById("frontPage");

const cycleImages = (images, container, step) => {
    images.forEach((image, index) => (
        setTimeout(() => {
            container.style.backgroundImage = `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.7)), url(${image})`
        }, step * (index + 1))
    ))
    setTimeout(() => cycleImages(images, container, step), step * images.length)
}

cycleImages(cats, node, 3000);