$(document).ready(function() {
    $(window).scroll(function() {
        let position = $(this).scrollTop();
        if (position > 40) {
            $(".language").slideUp("slow");
            $('.navbar').addClass('navAnimate');
        } else {
            $(".language").slideDown("fast");
            $('.navbar').removeClass('navAnimate');
        }
    });
});